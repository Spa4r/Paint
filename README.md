# Paint

## Description
Nous avons programmé à partir de zéro un Paint en JAVA.

## Remerciements
* **IUT Villetaneuse** - [site internet](https://iutv.univ-paris13.fr/)

## Auteurs
* **JETON Alex** - *Initial work* - [GitLab](https://gitlab.com/Spaar)

## License
Ce projet est sous licence MIT - voir le fichier [LICENSE](LICENSE) pour plus de détails.