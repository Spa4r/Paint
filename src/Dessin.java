import java.awt.Color;

public class Dessin {
    private int taille;
    private String forme;
    private Color couleur;
    private int abscisse;
    private int ordonnee;

    public Dessin(int taille,int abscisse,int ordonnee,String forme,Color couleur){
        this.setTaille(taille);
        this.setForme(forme);
        this.setCouleur(couleur);
        this.setAbscisse(abscisse);
        this.setOrdonnee(ordonnee);
    }

    public int getTaille(){
        return this.taille;
    }
    
    public int getAbscisse() {
        return this.abscisse;
    }

    public void setAbscisse(int abscisse) {
        this.abscisse = abscisse;
    }

    public int getOrdonnee() {
        return this.ordonnee;
    }

    public void setOrdonnee(int ordonnee) {
        this.ordonnee = ordonnee;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    public String getForme() {
        return this.forme;
    }

    public void setForme(String forme) {
        this.forme = forme;
    }

    public Color getCouleur() {
        return this.couleur;
    }

    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

    public String saveData() {
        return this.getTaille() + "," + this.getForme() + "," + this.getCouleur().getRGB() + "," + this.getAbscisse() + "," +this.getOrdonnee();
    }

    public String toString() {
        return "Taille : " + this.getTaille() + " Forme : " + this.getForme() + " Couleur : " + this.getCouleur().toString();
    }
}
