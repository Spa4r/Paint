import java.util.ArrayList;

public interface Donnees {
    public final static String[] couleurs = {"rouge","bleu","vert"};
    public final static String[] formes = {"cercle","carre"};
    public static ArrayList<Dessin> dessins = new ArrayList<Dessin>();
    public int choixDessins(int abscisse,int ordonnee);
}
